<?php
        class Tarea{
                public $fechaInicio;
                public $descripcion;
                public $duracion;
                public $observaciones;
                public $integrantes_id;

            function __construct()
            {
                date_default_timezone_set("America/Argentina/Buenos_Aires");
                $this->fechaInicio = date("Y-m-d H:i:s");
                    
            }

           public function getFechaIncio(){

                return $this->fechaInicio;

            }


            public function setFechaIncio($fec){
                $this->fechaInicio = $fec;
        }


            public function getDescripcion(){

                return $this->descripcion;

            }


            public function setDescripcion($desc){
                $this->descripcion = $desc;
        }


            public function getDuracion(){

                return $this->duracion;

            }



            public function setDuracion($dur){
                $this->duracion = $dur;
        }


            public function getObservaciones(){

                return $this->observaciones;

            }

            public function setObservaciones($obs){
                $this->observaciones = $obs;
        }


        public function getIntegrantes_id(){

            return $this->integrantes_id;

        }

        public function setIntegrantes_id($id){
            $this->integrantes_id = $id;
    }





        }//fin class Tarea















?>