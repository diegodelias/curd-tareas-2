<?php

require_once "conexion.php";

class CrudTareas{

	/*=============================================
	MOSTRAR USUARIOS
	=============================================*/

	static public function MostrarDatos(){ /**en hosting poner static */

		$conexionz = new Conexion();


			$tabla1 = "tareas";
			$tabla2= "integrantes";

			// $stmt = $conexionz->conectar()->prepare("SELECT * FROM $tabla");

			$stmt = $conexionz->conectar()->prepare("SELECT t.idtarea,t.fechaInicio,t.descripcion,t.duracion,t.observaciones,i.nombre from $tabla1 AS t INNER JOIN $tabla2 AS i  ON t.integrantes_id=i.id");

			$stmt -> execute();
	
			return $stmt -> fetchAll(); /**retorna todas la filas todos los usuarios */


	

		// }

		

		$stmt->close();
		$stmt = null;


	}


	
	static public function MostrarDatosDeUn($descripcion){ /**en hosting poner static */

		$conexionz = new Conexion();


			$tabla1 = "tareas";
			$tabla2= "integrantes";

			// $stmt = $conexionz->conectar()->prepare("SELECT * FROM $tabla");

			$stmt = $conexionz->conectar()->prepare("SELECT idtarea, fechaInicio, descripcion,duracion, observaciones, nombre from $tabla1 AS t WHERE idtarea= ");

			$stmt -> execute();
	
			return $stmt -> fetchAll(); /**retorna todas la filas todos los usuarios */


	

		// }

		

		$stmt->close();
		$stmt = null;


	}
	
	static public function MostrarNombresSelect(){ /**en hosting poner static */

		$conexionz = new Conexion();


		
			$tabla= "integrantes";

			$stmt = $conexionz->conectar()->prepare("SELECT * FROM $tabla");

			// $stmt = $conexionz->conectar()->prepare("SELECT t.idtarea,t.fechaInicio,t.descripcion,t.duracion,t.observaciones,i.nombre from $tabla1 AS t INNER JOIN $tabla2 AS i  ON t.integrantes_id=i.id");

			$stmt -> execute();
	
			return $stmt -> fetchAll(); /**retorna todas la filas todos los usuarios */


	

		// }

		

		$stmt->close();
		$stmt = null;


	}

	

	// crear tarea
	
	static public function crearTarea($tabla, $datos){
		try {
		$conexionz = new Conexion();
		$stmt = $conexionz->conectar()->prepare("INSERT INTO $tabla(fechaInicio,descripcion,duracion,observaciones,	integrantes_id) VALUES(:fechaInicio,:descripcion,:duracion,:observaciones, :integrantes_id)");


		$fecha = $datos->getFechaIncio();
		$descripcion = $datos->getDescripcion();
		$duracion = $datos->getDuracion();
		$observaciones = $datos->getObservaciones();
		$integrantes = $datos->getIntegrantes_id();

		// echo $tabla;

		$stmt -> bindParam(":fechaInicio",$fecha, PDO::PARAM_STR);
		$stmt -> bindParam(":descripcion",$descripcion, PDO::PARAM_STR);
		$stmt -> bindParam(":duracion",$duracion, PDO::PARAM_STR);
		$stmt -> bindParam(":observaciones", $observaciones , PDO::PARAM_STR);
		$stmt -> bindParam(	":integrantes_id", $integrantes, PDO::PARAM_INT);


	
		return $stmt->execute();
		$stmt->close();

		}catch (Exception $e) {
			echo $e->getMessage();
			die();
		}
		


	}


	static public function crearUsuario($tabla, $nombre){
		try {
		$conexionz = new Conexion();
		$stmt = $conexionz->conectar()->prepare("INSERT INTO $tabla(nombre) VALUES(:nombre)");


		$nombre = $nombre;
	

		// echo $tabla;

		$stmt -> bindParam(":nombre",$nombre, PDO::PARAM_STR);
	


	
		return $stmt->execute();
		$stmt->close();

		}catch (Exception $e) {
			echo $e->getMessage();
			die();
		}
		


	}



	//metodo editar usuario

	// static public function mdlEditarUsuario($tabla, $datos){

	// 	$conexionz = new Conexion();
		
	// 	$stmt = $conexionz->conectar()->prepare("UPDATE $tabla SET nombre = :nombre, password = :password, perfil = :perfil, foto = :foto WHERE usuario = :usuario");
	// 	// $stmt = Conexion::conectar()->prepare("UPDATE $tabla SET nombre = :nombre, password = :password, perfil = :perfil, foto = :foto WHERE usuario = :usuario");

	// 	$stmt -> bindParam(":nombre", $datos["nombre"], PDO::PARAM_STR);
	// 	$stmt -> bindParam(":password", $datos["password"], PDO::PARAM_STR);
	// 	$stmt -> bindParam(":perfil", $datos["perfil"], PDO::PARAM_STR);
	// 	$stmt -> bindParam(":foto", $datos["foto"], PDO::PARAM_STR);
	// 	$stmt -> bindParam(":usuario", $datos["usuario"], PDO::PARAM_STR);

	// 	if($stmt->execute()){
	// 		return "ok";


	// 	}else {

	// 		return "error";
	
	// 	}

	// 	$stmt->close();
	// 	$stmt = null;


	// }




	// static public function mdlActualizarUsuario($tabla, $item1,$valor1,$item2,$valor2){/**METOD PARA ACTUALIZAR ESTADO DEL USUARIO EN LA BASE DE DATOS */
	// 	$conexionz = new Conexion();
	// 	// item1 estado, item2 id  se actualizar id de la tabla
	// 	$stmt = $conexionz->conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE $item2 = :$item2"); /**va actualizar el estado correspondiente al id que se le pas por parametro */
		
	// 	$stmt -> bindParam(":".$item1,$valor1, PDO::PARAM_STR);
	// 	$stmt -> bindParam(":".$item2,$valor2, PDO::PARAM_STR);
	// 	if($stmt -> execute()){
	// 		return "ok";
	// 	} else{
	// 		return "error";

	// 	}

	// 	$stmt -> close();
	// 	$stmt = null;

	// }


// Borrar Usuario


// static public function mdlBorrarUsuario($tabla, $datos){

// 	$conexionz = new Conexion();

// 	$stmt = $conexionz->conectar()->prepare("DELETE FROM $tabla WHERE id = :id");
// 	$stmt -> bindParam(":id",$datos, PDO::PARAM_INT); /**BINDEA parmetro  datos con
// 	"id" en la consulta */


// 	if($stmt -> execute()){
// 		return "ok";
// 	} else{
// 		return "error";

// 	}

// 	$stmt -> close();
// 	$stmt = null;

// }

	

}