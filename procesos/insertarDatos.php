<?php
require_once "../crud/CrudTareas.php";
require_once "../crud/ClaseTarea.php";

$tarea = new Tarea;
$tabla = "tareas";

$integrante= (int)$_POST['responsable'];

$tarea->setDescripcion($_POST['descripcion']);
$tarea->setDuracion($_POST['duracion']);

$tarea->setObservaciones($_POST['observaciones']);
$tarea->setIntegrantes_id($integrante);
$tarea->setFechaIncio($_POST['fecha']);

echo CrudTareas::crearTarea($tabla,$tarea);


?>