<!-- Modal -->
<div class="modal fade" id="actualizarModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Actualizar registro</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="frminsertu" onsubmit="return actualizarDatos()" method="post">
            <input type="text" id="id" name="id" hidden="">
              <label>Nombre</label>
              <input type="date" id="fecha" name="nombreu" class="form-control form-control-sm" required="">
              <label>Fecha Inicio</label>
              <input type="
              ext" id="fecha" name="fecha" class="form-control form-control-sm" required="">
              <label>Descripcion</label>
              <input type="text" id="descripcion" name="descripcion" class="form-control form-control-sm">
              <label>Duracion(días)</label>
              <input type="text" id="duracion" name="duracion" class="form-control form-control-sm">

              <label>Observaciones</label>
              <input type="text" id="observaciones" name="observaciones" class="form-control form-control-sm">

              <label>Tarea asignada a:</label>
              <select class="browser-default custom-select custom-select-lg mb-3" id="responsable" name="responsable">


              <br>
               <input type="submit" value="Actualizar" class="btn btn-warning">
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>
