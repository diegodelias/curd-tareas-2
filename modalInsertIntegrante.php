<div class="modal fade" id="insertarUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Insertar nuevo Integrante</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
    
          <form id="frminsertInt" onsubmit="return insertarNuevoIntegrante()" method="post">
            <input type="text" id="id" name="id" hidden="">
              <label>Nombre y apellido</label>
              <input type="text" id="nombreI" name="nombreI" class="form-control form-control-sm" required="">


              <br>
               <input type="submit" value="Guardar"  class="btn btn-primary">
        
          </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onClick="window.location.reload();"  data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>
